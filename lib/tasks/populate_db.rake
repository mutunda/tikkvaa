require 'faker'

namespace :db do

  desc 'check evn in dev'
  task ensure_development_environment: :environment do
    if Rails.env.production?
      raise "\nI'm sorry, I can't do that.\n(You're asking me to drop your production database.)"
    else
      puts "#{Rails.env}"
    end

  end

  desc 'inset fake contacts into db'
  task populate: :environment do
    # Avoid duplication so delete everything in from the DB
    Rake::Task['db:reset'].invoke
    puts 'inserting a 1000 records...'
    count = 0
    1000.times do
      Contact.create( first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, eeid: Faker::Number.number(11),
                     phone_number: Faker::PhoneNumber.cell_phone )
      count += 1
      puts "#{count}"
    end
    puts 'done.....'
  end

end

