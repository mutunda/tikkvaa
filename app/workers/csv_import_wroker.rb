class CSVImportWroker
  include Sidekiq::Worker
  # sidekiq_options queue: 'import'

  def perform(file_path, file_name)
    address_book = AddressBook.find_or_create_by!(name: "#{file_name.chomp('.csv').strip}")
    contact_ids = []

    CSV.foreach(file_path, headers: true) do |row|
      eeid = row['eeid']
      # raise
      contact = Contact.create_with(eeid: eeid).find_or_create_by(row.to_hash)
      contact_ids << contact.id
    end
    return address_book.contact_ids = contact_ids
  end

end