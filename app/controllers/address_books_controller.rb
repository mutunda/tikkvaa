class AddressBooksController < ApplicationController
  before_action :set_address_book, only: [:show, :edit, :update, :destroy]

  def index
    @address_books = AddressBook.order('updated_at DESC')
    respond_to do |format|
      format.html
      # format.csv { send_data  @address_books.all_to_csv }
    end

  end

  def show
    @contacts = @address_book.contacts
    respond_to do |format|
      format.html
      format.csv {send_data @address_book.to_csv, filename: "#{@address_book.name}.csv"}
    end
  end

  def new
    @address_book = AddressBook.new
  end

  def edit
  end

  def create
    @address_book = AddressBook.new(address_book_params)

    respond_to do |format|
      if @address_book.save
        format.html {redirect_to @address_book, notice: 'Address book was successfully created.'}
        format.json {render :show, status: :created, location: @address_book}
      else
        format.html {render :new}
        format.json {render json: @address_book.errors, status: :unprocessable_entity}
      end
    end
  end

  def import
    if params[:file].present?
      csv_file = params[:file]
      CSVImportWroker.perform_async(csv_file.path, csv_file.original_filename)
      redirect_to address_books_path, notice: "Successfully uploaded file #{csv_file.original_filename.chomp('.csv').strip}"
    else
      redirect_to address_books_path, notice: 'Nothing imported...'
    end

  end


  def update
    respond_to do |format|
      if @address_book.update(address_book_params)
        format.html {redirect_to @address_book, notice: 'Address book was successfully updated.'}
        format.json {render :show, status: :ok, location: @address_book}
      else
        format.html {render :edit}
        format.json {render json: @address_book.errors, status: :unprocessable_entity}
      end
    end
  end


  def destroy
    @address_book.destroy
    respond_to do |format|
      format.html {redirect_to address_books_url, notice: 'Address book was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
  def set_address_book
    @address_book = AddressBook.find(params[:id])
  end

  def address_book_params
    params.require(:address_book).permit(:name, :contact_ids => [])
  end

  def csv_file_param
    # params.require
  end
end
