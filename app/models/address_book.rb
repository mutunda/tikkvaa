class AddressBook < ApplicationRecord
  validates_presence_of :name
  validates_uniqueness_of :name
  validates :name, length: {in: 4..30}

  has_and_belongs_to_many :contacts

  def to_csv
    contacts = self.contacts.all
    attributes = %w{first_name last_name eeid phone_number}.freeze
    CSV.generate(headers: true) do |csv|
      csv << attributes
      contacts.each do |contact|
        csv << contact.attributes.values_at(*attributes)
      end
    end
  end

  def self.import(file)
    # CSVWorker.perform_async(file)
    #
    # address_book = find_or_create_by!(name: "#{file.original_filename.chomp('.csv').strip}")
    # contact_ids = []
    #
    # CSV.foreach(file.path, headers: true) do |row|
    #   eeid = row['eeid']
    #   contact = Contact.create_with(eeid: eeid).find_or_create_by(row.to_hash)
    #   contact.update_columns!( first_name: contact.first_name, last_name: contact.last_name ) unless contact.new_record?
    #   contact_ids << contact.id
    # end
    #
    # return address_book.contact_ids = contact_ids
  end

  # def self.all_to_csv
  #   all.each do |address_book|
  #     address_book.to_csv
  #   end
  # end

end
