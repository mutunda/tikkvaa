class Contact < ApplicationRecord

  include PgSearch

  validates_presence_of :first_name, :last_name , :phone_number ,:eeid
  validates_uniqueness_of :phone_number, :eeid
  validates  :eeid , length: { is:11 }
  validates  :phone_number, length: { in: 8..13 }

  has_and_belongs_to_many :address_books

  pg_search_scope :search ,
                  against: [:first_name, :last_name],
                  using:  {
                      tsearch: { prefix: true, dictionary: "english"}
                  },
                  ignoring: :accents

end