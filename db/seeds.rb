require 'faker'

puts 'inserting a 1000 records...'
         1000.times do

  Contact.create(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, eeid: Faker::Number.number(11),
                 phone_number: Faker::PhoneNumber.cell_phone)

end
puts 'done'