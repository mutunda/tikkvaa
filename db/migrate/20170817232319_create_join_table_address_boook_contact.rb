class CreateJoinTableAddressBoookContact < ActiveRecord::Migration[5.1]
  def change
    create_join_table :address_books, :contacts do |t|
      t.index [:address_book_id, :contact_id]
      # t.index [:contact_id, :address_book_id]
    end
  end
end
