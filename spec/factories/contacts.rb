FactoryGirl.define do
  factory :contact do
    first_name "MyString"
    last_name "MyString"
    phone_number "MyString"
    eeid "MyString"
  end
end
