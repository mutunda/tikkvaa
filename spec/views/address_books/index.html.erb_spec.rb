require 'rails_helper'

RSpec.describe "address_books/index", type: :view do
  before(:each) do
    assign(:address_books, [
      AddressBook.create!(
        :name => "Name"
      ),
      AddressBook.create!(
        :name => "Name"
      )
    ])
  end

  it "renders a list of address_books" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
  end
end
