require 'rails_helper'

RSpec.describe "contacts/index", type: :view do
  before(:each) do
    assign(:contacts, [
      Contact.create!(
        :first_name => "First Name",
        :last_name => "Last Name",
        :phone_number => "Phone Number",
        :eeid => "Eeid"
      ),
      Contact.create!(
        :first_name => "First Name",
        :last_name => "Last Name",
        :phone_number => "Phone Number",
        :eeid => "Eeid"
      )
    ])
  end

  it "renders a list of contacts" do
    render
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "Phone Number".to_s, :count => 2
    assert_select "tr>td", :text => "Eeid".to_s, :count => 2
  end
end
