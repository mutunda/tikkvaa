require 'rails_helper'

RSpec.describe AddressBook, type: :model do

  context 'Validation' do
    it { should validate_presence_of :name }
    it { should validate_length_of :name }
  end


end
