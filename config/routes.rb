require 'sidekiq/web'

Rails.application.routes.draw do

  mount Sidekiq::Web, at: '/sidekiq'

  resources :contacts
  resources :address_books do
    collection { post :import }
  end
  get 'home/index'
  root 'home#index'

end
